var register  = document.getElementById('register'), // блок с регистрацией
    open_btn  = document.getElementById('open'), // кнопка с открытием
    close_btn = document.getElementById('close'),    // кнопка с закрытием
    isOpened  = false; // открыто ли окно
open_btn.onclick = function(){
    // открытие окна с регистрацией
    register.style.display = block;
    isOpened = true;
}
close_btn.onclick = function(){
    // закрытие окна по кнопки с закрытием
    register.style.display = none;
    isOpened = false;
}
if(isOpened){
    document.onclick = function(e){
        if(e.target.id !== 'register'){
            // закрытие при клике вне поля регистрации
            register.style.display = 'none';
            isOpened = false;
        }
    }
}